
output: gshell.o history
	g++ gshell.o -o gshell

gshell.o: gshell.cpp
	g++ -c  gshell.cpp

history: history.cpp
	g++ -o history history.cpp

clean:
	rm *.o
