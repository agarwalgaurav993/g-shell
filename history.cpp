#include "functions.h"
using namespace std;
/*History Command*/
vector<pair<string,string> > cmd;
void split(string &s, char delim, pair<string, string> &elems) {
    istringstream ss;
    ss.str(s);
    string item;
    getline(ss, item, delim) ;
        elems.first=item;
    getline(ss, item, delim) ;
        elems.second=item;
}
void populate()
{
	cmd.clear();
	
	ifstream t;
	t.open(".history",std::ios::in | std::ios::app);
	string line;
	pair <string,string> elems;
	while(!t.eof())
	{
		getline(t, line,'\n');
		split(line,'@',elems);
		cmd.push_back(elems);


	}
	t.close();

	return;
}


int main(int argc, char* argv[])
{
	populate();
	if(argc == 1)
	{
		//Just Print
		vector <pair<string,string> > :: iterator iter;
		for(iter = cmd.begin() ; iter!=cmd.end(); iter++)
			cout << (*iter).first<<"\t"<<(*iter).second<<"\n";
		
		return 0;
	}
	if(argv[1][0] != '!')
	{
		bool temp = (string(argv[1]).find_first_not_of("0123456789") == string::npos);
		int num = atoi(argv[1]);
		if(temp==false){
			cout<<"gshell: history: "<<argv[1]<<": numeric argument required\n";
		}
		
		for(int i = cmd.size() - num -1; i<cmd.size();i++)
			cout <<cmd[i].first<<"\t"<<cmd[i].second<<endl;
		//convet to int the number

	}
	return 0;
}


