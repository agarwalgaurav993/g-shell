#include "functions.h"
using namespace std;
int ind = 1;
vector<string> his;
void log(string operation)
{
	ind++;
  stringstream ss;
  ss << ind;
  //string str = ss.str();
	string l =  ss.str() + "@"+ operation + '\n';
	his.push_back(operation);
	fstream fs;
  	fs.open (".history",fstream::out|fstream::app);
  	fs << l;
  	fs.close();
}

void split(string &s, char delim, pair<string, string> &elems) {
    istringstream ss;
    
    ss.str(s);
    string item;
    getline(ss, item, delim) ;
        elems.first=item;
    getline(ss, item, delim) ;
        elems.second=item;
}
void populate()
{
  his.clear();
  
  ifstream t;
  
  t.open(".history",std::ios::in | std::ios::app);
  string line;
  
  pair <string,string> elems;
  while(!t.eof())
  {
    getline(t, line,'\n');
    
    split(line,'@',elems);
    stringstream ss(elems.first);
    ss >> ind;
    his.push_back(elems.second);

  }
  his.pop_back();
  t.close();

  return;
}
string find(string s)
{
  
  if(s == "!")
      return his.back();

  string toExecute;
  for(int i = his.size()-1 ; i>=0; i--)
    if((his[i].find(s) + 1)==1)
        return his[i];
  toExecute[0] = '\0';
  return toExecute;
}
int exclamationexpand(string &cmd)
{
  int i = 0;
  int k;
  
  while(cmd[i] != '\0' && cmd[i] !='\n' ){
    while(cmd[i]!='!' && cmd[i]!='\0' &&  cmd[i] !='\n' )
          i++;
    if(cmd[i]=='!')
    {
    
      int pos = i;
      string a;
      i++;
      int end;
      while(cmd[i]!=' ' && cmd[i]!='\0'){
        i++;
      }
      end = i-1;
      string replace = cmd.substr (pos+1, end - pos);
      
      a = find(replace);
        cmd.replace(pos, i - pos,a);
        cout<<"\n"<<cmd<<endl;
      return 1;
    }
  }
  return 0;
}
/*int main()
{
    populate();
    /*for(int i = 0;i<his.size();i++)
      cout<<his[i]<<"\n";
    string cmd;
    getline(cin,cmd);
    //cout<<cmd<<"\n";
    /*for(int i=0;cmd[i]!='\0';i++){
      cout<<cmd[i];
    }

    exclamationexpand(cmd);
    //cout<<cmd<<endl;
    log(cmd);
}*/
