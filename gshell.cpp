
#include "functions.h"
#include "history.h"
using namespace std;
size_t len;
char npath[1024];
int flag = 0;
char *prev;
int saved_stdin;
vector <pair<int,int> > jobs;
int jobnum = 0;

void eHandle(char *msg)
{
	perror(msg);
	return;
}
void sigHandleAsChild(int sig)
{
  if(sig == SIGINT || sig == SIGTSTP)
  {
      exit(1);
  }
  else if(sig == SIGHUP)
  {
  		//dup2(saved_stdin, STDIN_FILENO);
  		dup2(3,0);
  }
}
void sigHandleAsParent(int sig)
{
    if(sig  == SIGINT || sig == SIGTSTP )
    {
        return;
    }
}
void sigHandleAsShell(int sig)
{
    if(sig  == SIGINT || sig == SIGTSTP)
    {
        //cout<<"\n";
        return;
    }
}

/*Parsging Logic*/
void insertspace(string &cmd)
{
	int i = 0;
	
	while(cmd[i] != '\0' ){
		if(cmd[i] == '|')
			flag=1;
		if(cmd[i] == '>' || cmd[i] == '<' || cmd[i] == '|' || cmd[i] == '=' ){
			cmd.insert( i , 1 ,' ');
			cmd.insert( i+2 , 1 ,' ');
			i = i+1;

		}
		else if(cmd[i]=='$' || cmd[i] == '&')
		{
			cmd.insert( i , 1 ,' ');
			i++;
		}
		i++;
	}
	
	
	//cout<<"Spaced command : "<<cmd;
}
int parse(char* line, char* argv[])
{
	int i=0;
	int k=0;
	while(line[i] != '\0')
	{
		while(line[i] == ' ' || line[i] == '\t' || line[i] == '\n')
		{
			line[i++] = '\0';
		}
		argv[k++] = (char*) &line[i];
		while(line[i] != '\0' && line[i] != ' ' && line[i] != '\n')
			i++;
	}
	argv[k]='\0';

	/*EXPANDING ENV VARIABLES*/
	for(i=0;i<k;i++)
	{
		if(argv[i][0]=='$')
		{
			argv[i]=getenv(&argv[i][1]);
			if(argv[i]==NULL){
				argv[i] = "\n";
			}
		}
	}

	return k;
}

//Execute Logic Multi-Pipes
void execute(char *argv[],int k)
{
	int t = k -1;
	int fileWrite;
	char *filename;
	int background=0;

	/*Background Requirement Detection*/
	/*while(t>=0 && argv[t][0] != '&')
		t--;*/
	if(argv[t][0] == '&')
	{
		
		background = 1;
		argv[t]='\0';
		t--;
	}
	while(t>=0 && argv[t][0] != '>')
		t--;

	/*Output Redirection Handling*/
	int file;
	if(argv[t][0] == '>'){
		fileWrite = 1;
		filename = argv[t+1];
		argv[t]='\0';
		
	}
	else
	{
		fileWrite = 0;
		filename = NULL;
		t=k;
	}
	
	/*To get the command this process has to execute*/

	/*Process creation to subsequent command run*/
	pid_t  pid;
  	int status;
  	int pipefd[2] = {0,1};
  	int saved_stdout;
  	
  	//dup2(STDOUT_FILENO, saved_stdin);
    //dup2(STDIN_FILENO, saved_stdout);
  	if(fileWrite == 1){
  		cout<<filename;
  		file = open(filename, O_CREAT|O_WRONLY|O_TRUNC,0777);
  		if(file < 0)
  		{
			perror(filename);
  		}
  		dup2(file,1);
	}
	//cout<<"Bg SET 1\n";
    /*Single Command Execution*/
    if(flag==0)
    {
    	pid = fork();
  		if(pid == 0)
  		{
  			signal(SIGINT, sigHandleAsChild);
        	signal(SIGTSTP, sigHandleAsChild);
        	signal(SIGHUP, sigHandleAsChild);
        	if(background == 1){
        		//cout<<"Bg SET 1\n";
        		dup2(0, 3);
        		//close(STDIN_FILENO);
        	}
  			if (execvp(argv[0], argv) < 0)
			{
	           cout<<argv[0]<<": command not found\n";
	           exit(1);
			}
  		}
  		else
  		{
        	if(background == 0){
	  			while (wait(&status) != pid)
	  			;
	  		}
	  		else{
	  			jobs.push_back(make_pair(pid,++jobnum));
	  			//message
	  			cout<<"["<<jobnum<<"]\t"<<pid<<endl;
	  		}
  			signal(SIGINT, sigHandleAsShell);
        	signal(SIGTSTP, sigHandleAsShell);
  		}
  		goto final;
    }

  	pid = fork();
  	if(pid == 0)
  	{
  		signal(SIGINT, SIG_DFL);
        signal(SIGTSTP, SIG_DFL);
  		char **myargv;
  		int prfd, pwfd;
  		label:
  		prfd = pipefd[0];
  		pwfd = pipefd[1];
  		close(prfd);
  		
  		/*Process that I have to execute*/
  		while(t>0 && argv[t-1][0]!= '|')
			t--;
		argv[t-1] = '\0';
		myargv = argv + t;

		if(pipe(pipefd)<0){
	    		eHandle("pipe");
	    		return;
  		}

  		if(t!=0)/*Indicating more processes to follow*/
		{
			
			t--;
			pid = fork();
			if(pid==0)
				goto label;
			else
			{
				while (wait(&status) != pid)
  					;
  				
			}
		}
		close(pipefd[1]);/*My Pipe Write End*/
  		dup2(pipefd[0], 0);
		dup2(pwfd, 1);
		if (execvp(myargv[0], myargv) < 0)
		{
           eHandle(myargv[0]);
           exit(1);
		}
		
		close(pipefd[0]);
		close(pwfd);

  	}
  	else
  	{
  		signal(SIGINT, sigHandleAsParent);
        signal(SIGTSTP, sigHandleAsParent);
  		/*Parent Waiting for child to return*/
  		while (wait(&status) != pid)
  			;    
        signal(SIGINT, sigHandleAsChild);
        signal(SIGTSTP, sigHandleAsChild);   
  	}

  	final:
  	if(fileWrite == 1)
  	{
  		
  		close(file);
  		dup2(saved_stdout,1);
  	}
  	//dup2(saved_stdout, STDOUT_FILENO);
    dup2(saved_stdin, STDIN_FILENO);
    dup2(saved_stdout, STDOUT_FILENO);
    flag = 0;
  	return;
}

int executeIfBuiltin(char *argv[])
{
	if(strcmp(argv[0],"cd") == 0)
	{
		if(chdir(argv[1])!=0)
		{
			eHandle(argv[0]);	
		}
	}
	else if(strcmp(argv[0], "pwd") == 0)
	{
		cout<<getcwd(npath,1024)<<endl;
	}
	else if(strcmp(argv[0], "export") == 0)
	{
		if(setenv(argv[1],argv[3],1)<0)
		{
			eHandle(argv[0]);
		}
	}
	else if(strcmp(argv[0], "fg") == 0)
	{
		if(argv[1] == '\0')
			cout<<"gshell : fg: current: no such job\n";
		else if(argv[1][0] = '#'){

		}
		else
		{
			int jno = atoi(argv[1]);
			//find job
			vector <pair<int,int> > :: iterator iter;
			for(iter = jobs.begin(); iter < jobs.end(); iter++)
				if((*iter).second == jno){
					int status;
					kill((*iter).first,SIGHUP); 
					waitpid((*iter).first,&status,WNOHANG|WUNTRACED);
					jobs.erase(iter);
					break;
				}
		}
	}
	else
		return 1;
	return 0;

}


void bookkeeping()
{
	prev = getenv("PATH");
	char *n = strcat(prev,":");
	n = strcat(n,getenv("PWD"));
	if(setenv("PATH",n,1)<0)
	{
		eHandle("PATH");
	}
}
void restore()
{
	if(setenv("PATH",prev,1)<0)
	{
		eHandle("PATH");
	}
}


int main(int argc, char *inp[])
{

    signal (SIGINT, SIG_IGN);
    signal (SIGTSTP, SIG_IGN);
      
     string cmd;
     char*  argv[64];
     //char ar[1024];
     populate();
     cout<<"\n*********************************************************\n";
     cout<<"\t\tWELCOME TO GSHELL\n";
     cout<<"\n*********************************************************\n\n";
     char *cmdf;
     bookkeeping();
     while (1) {
     	signal (SIGINT, SIG_IGN);
      	signal (SIGTSTP, SIG_IGN);
	      cout<<"gshell:"<<getcwd(npath,1024)<<"$  ";
	      getline(cin,cmd);
	      if(cmd == "\0")
	      	continue;

	      /*History Error*/
	      exclamationexpand(cmd);
	      log(cmd);


	      insertspace(cmd);
	      cmdf = strdup(cmd.c_str());
	      int tokens = parse(cmdf, argv);
          if (string(argv[0]) == "exit"){

               goto over;
          }
			
          if(executeIfBuiltin(argv))
          	execute(argv,tokens);

     }
     over:
     restore();
     exit(0);
     return 0;
}